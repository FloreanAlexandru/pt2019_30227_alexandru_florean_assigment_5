package main;
import java.io.*;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class MonitoredData {
	
	private Date start_time;
	private String activity_label;
	private Date end_time;
	
	public MonitoredData(Date start_time, String activity_label, Date end_time) {
		super();
		this.start_time = start_time;
		this.activity_label = activity_label;
		this.end_time = end_time;
	}

	public String getActivity_label() {
		return activity_label;
	}

	public void setActivity_label(String activity_label) {
		this.activity_label = activity_label;
	}

	public Date getStart_time() {
		return start_time;
	}

	public void setStart_time(Date start_time) {
		this.start_time = start_time;
	}

	public Date getEnd_time() {
		return end_time;
	}

	public void setEnd_time(Date end_time) {
		this.end_time = end_time;
	}

	@Override
	public String toString() {
		return "MonitoredData\n start_time=" + start_time + " activity_label=" + activity_label + " end_time="
				+ end_time ;
	}

	public long getDuration() {
		return this.getEnd_time().getTime() - this.getStart_time().getTime();
	}
	
	public long getMinutes() {
		
		long a = this.getDuration();
		a= TimeUnit.MILLISECONDS.toMinutes(a);
		
		return a;
	}
}
