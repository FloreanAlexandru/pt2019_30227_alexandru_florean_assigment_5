package main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;



/*		File path = new File("Activities.txt");

BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(path)));


String line = reader.readLine();

while (line != null) {
	
	System.out.println("Read a line: "+ line);
	line = reader.readLine(); 
	
}

reader.close();
*/


public class MainClass {
	
	//Subpunctul 1
	
	public ArrayList<MonitoredData> cerinta1(Stream<String> stream,ArrayList<MonitoredData> date) throws ParseException {
		SimpleDateFormat format;
		format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		List<String[]> lista = stream.map(x->x.split("\t\t")).collect(Collectors.toList());

		Date dataInceput;
		Date dataSfarsit;
		
		String activitate;
		MonitoredData x= null;
		
		//Prima cerinta
		int i=0;
		for(String[] item: lista) {
			System.out.println(item[0]+"\n"+item[1]+"\n"+item[2]+"\n");
		
			dataInceput = format.parse(item[0]);
			dataSfarsit = format.parse(item[1]);
			activitate = item[2];
			
			x=new MonitoredData(dataInceput,activitate,dataSfarsit);
			
			date.add(x);
			
			i++;
		}
		
		System.out.println("\nNumarul de activitati: " + i);
		//System.out.println("\nAfisare date\n"+date.get(0).getStart_time().toString()+"\n si aici\n"+date.get(0).getEnd_time().toString());
		
		return date;
	}

	//Subpunctul 2
	public void cerinta2(ArrayList<MonitoredData> date) {
		
		ArrayList<Integer> dati = new ArrayList<>();

		for(MonitoredData i : date) {
			dati.add(i.getEnd_time().getDate());
		}

		int days = (int) dati.parallelStream().distinct().count();
		//int days = (int) date.stream().map(y->y.getEnd_time().getDate()).distinct().count();
		
		System.out.println("\n Rezultatul cerintei 2 -> " +days);
	}
	
	//Subpunctul 3
	
	public void cerinta3(ArrayList<MonitoredData> date,Stream<String> stream) {
		
		String act= "";
		
		for(MonitoredData i : date) {
			act=act + " " + i.getActivity_label();
		}
	    
		List <String> list = Stream.of(act).map(w -> w.split("\\s+")).flatMap(Arrays::stream)
	            .collect(Collectors.toList());
			      
		Map<String, Integer> activitati = list.stream()
	            .collect(Collectors.toMap(w -> w, w -> 1,Integer::sum));
	 
		for(Entry<String, Integer> entry : activitati.entrySet()) {
	        String nume = entry.getKey();
	        Integer nr_executari = entry.getValue();
		
	        if(!nume.equalsIgnoreCase("")) {
	        System.out.println(nr_executari +" -> "+nume);
	        }
		}

		
}
	
public void cerinta4(ArrayList<MonitoredData> date,Stream<String> stream) {
		
		List<Integer> lista= new ArrayList<Integer>();
		lista = date.stream().map(x->{return x.getStart_time().getDate();}).distinct().collect(Collectors.toList());
		
		//System.out.println(lista + "aici");
		
		for(Integer i: lista) {
			System.out.println("\nIn data de: "+i+"\n");
		
			String act= "";
			for(MonitoredData j : date) {
				if(i == j.getStart_time().getDate()) {
				act=act + " " + j.getActivity_label();
			}
		}
			List <String> list = null;
			list = Stream.of(act).map(w -> w.split("\\s+")).flatMap(Arrays::stream)
		            .collect(Collectors.toList());
				      
			Map<String, Integer> activitati = list.stream()
		            .collect(Collectors.toMap(w -> w, w -> 1,Integer::sum));
		 
			//Afisare
			for(Entry<String, Integer> entry : activitati.entrySet()) {
		        String nume = entry.getKey();
		        Integer nr_executari = entry.getValue();
			
		        if(!nume.equalsIgnoreCase("")) {
			        System.out.println(nr_executari +" -> "+nume);
			        }
			}
			
			//activitati.forEach(it -> System.out.println(" > " + it));
	    }
}

	
	public void cerinta5(ArrayList<MonitoredData> date) {
		
		long act;
		long act1;
		long dif,h,m,s;
		for(MonitoredData i : date) {
			act=i.getStart_time().getTime();
			act1= i.getEnd_time().getTime();
//			System.out.println(act+ " " + act1);
			dif= act1 - act;
			
			h = TimeUnit.MILLISECONDS.toHours(dif);
			//System.out.println(dif +" aici in ore "+ h);
			
			dif = dif - h * 60 * 60 * 1000;

			m = TimeUnit.MILLISECONDS.toMinutes(dif);

			//System.out.println("Noua dif"+dif+" minutele "+m);
			
			dif = dif - h * 60 - m*60*1000;
			s = TimeUnit.MILLISECONDS.toSeconds(dif);
			
			System.out.println(i.getActivity_label()+" in "+h+"ore "+m+" minute si "+s+" secunde");
		} 
	}
	
	public void cerinta6(ArrayList<MonitoredData> date) {
		
		long suma =0;
		List<String> activitati = Arrays.asList("Sleeping","Toileting","Showering","Breakfast","Grooming","Spare_Time/TV","Leaving","Lunch","Snack");
		for(String s : activitati) {
			suma =0;
			for(MonitoredData a : date) {
				//System.out.println(s+" ACI "+a.getActivity_label());
				if(a.getActivity_label().contains(s)) {
					suma=suma + a.getEnd_time().getTime()-a.getStart_time().getTime();
				}
			}
			
			long h = TimeUnit.MILLISECONDS.toHours(suma);
			suma = suma - h * 60 * 60 * 1000;

			long m = TimeUnit.MILLISECONDS.toMinutes(suma);
			suma = suma - h * 60 - m*60*1000;
			
			long sec = TimeUnit.MILLISECONDS.toSeconds(suma);
			
			System.out.println(s+" in "+h+"ore "+m+" minute si "+sec+" secunde");
		}
	}
	
	public void cerinta7(ArrayList<MonitoredData> date) {
		
		Map<String,Long> act = date.parallelStream()
				.collect(Collectors.groupingByConcurrent
				(MonitoredData:: getActivity_label,Collectors.counting()));
			
		Map<String,Long> first = date.parallelStream().
				filter(x->(x.getMinutes())<=5).
				collect(Collectors.groupingByConcurrent(MonitoredData::getActivity_label,Collectors.counting()));
	
		List<String> second = first.entrySet().parallelStream().
				filter(x->x.getValue() >= 0.9 * act.get(x.getKey())).
				map(y->y.getKey()).
				collect(Collectors.toList());
		

		
		
		second.forEach(System.out::println);
	
	}
	
	public static void main (String[] args) throws IOException, ParseException {

		String cale = "Activities.txt";
		ArrayList<MonitoredData> date = new ArrayList<MonitoredData>();
		Stream<String> stream = Files.lines(Paths.get(cale));
		
		//stream.forEach(System.out::println);
		
		MainClass main = new MainClass();
		
		Map<String,Integer> activitati = null;
		
		//Cerintele ->
//1		
		//date=main.cerinta1(stream, date);
//2		
		//main.cerinta2(date);
//3
	 	//main.cerinta3(date,stream);
//4
		//main.cerinta4(date,stream);
//5
	 	//main.cerinta5(date);
//6 
		//main.cerinta6(date);
//7 ..
		//main.cerinta7(date);
	}
	
	
}